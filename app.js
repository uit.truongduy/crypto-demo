const express = require("express");
const routers = require("./server/routers");
require('dotenv').config()

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.get("/", (req, res, next) => res.status(200).json({message: "ok"}))
app.use("/api", routers)

// Catch 404 error & forward to handler
app.use((req, res, next) => {
  const err = new Error('Not Found')
  err.status = 404
  return next(err)
})

// error handler
app.use(function (err, req, res, next) {
  const {status, message, errors, errorCode} = err;
  return res.status(status || 500).json({
    message: message,
    errors: errors,
    errorCode
  });
});



const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Server listening on port ${port}`)
})