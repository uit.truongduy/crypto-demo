require('dotenv').config()

module.exports = {
  network: 'testnet',
  host: '0.0.0.0',
  port: 18332,
  username: process.env.RPC_USER,
  password: process.env.RPC_PASSWORD,
}