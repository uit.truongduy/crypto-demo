const web3 = require("../../utils/node.eth");
const datas = require("../../../data/account.json");
const { responseToClient } = require("../../utils/response.util");

exports.createAccount = async (req, res, next) => {
  try {
    const account = web3.eth.accounts.create();

    const { address, privateKey } = account;
    datas.push({
      address,
      privateKey,
    });

    return responseToClient(res, { account });
  } catch (error) {
    next(error);
  }
};
