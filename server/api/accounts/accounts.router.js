const express = require("express");
const accountController = require("./accounts.controller");

const router = express.Router();

router.post("/", accountController.createAccount);

module.exports = router;
