const { responseToClient } = require("../../utils/response.util");
const transactionService = require("./transaction.service");
const web3 = require("../../utils/node.eth");
const axios = require("axios");
const endpoints = require("../../../configs/gasTracker.config").endpoints;
const { ABI, contractAddress } = require("../../../contracts/contract");
const Tx = require("ethereumjs-tx").Transaction;
const { default: BigNumber } = require("bignumber.js");
const api = require("../../utils/node.xrp");
const xrp_account = require("../../../data/xrp_account.json");

exports.getTransaction = async (req, res, next) => {
  try {
    const { address } = req.query;
    const transaction = await transactionService.getTransactionDetail(address);

    return responseToClient(res, { data: transaction });
  } catch (error) {
    next(error);
  }
};

exports.estimateFee = async (req, res, next) => {
  try {
    const { input = 1, output = 2 } = req.query;
    const estimateFee = await transactionService.estimateFee(input, output);

    return responseToClient(res, { data: estimateFee });
  } catch (error) {
    next(error);
  }
};

exports.createRawTransaction = async (req, res, next) => {
  try {
    const { inputs, outputs } = req.body;

    const rawTransaction = await transactionService.createRawTransaction(
      inputs,
      outputs
    );
    return responseToClient(res, { data: { hexString: rawTransaction } });
  } catch (error) {
    next(error);
  }
};

exports.signRawTransactionWithKey = async (req, res, next) => {
  try {
    const { hexString } = req.body;
    const signTransaction = await transactionService.signRawTransactionWithKey(
      hexString,
      "my pass phrase",
      "tb1qx7ey2xyn6avz4w0z7uyju9qdge86yqzawq5qvv"
    );
    return responseToClient(res, { data: signTransaction });
  } catch (error) {
    next(error);
  }
};

exports.sendRawTransaction = async (req, res, next) => {
  try {
    const { hexString } = req.body;
    const send = await transactionService.sendRawTransaction(hexString);
    return responseToClient(res, { data: send });
  } catch (error) {
    next(error);
  }
};

exports.createTransaction = async (req, res, next) => {
  try {
    const { to, amount } = req.body;
    const from = "tb1q3xs7kysalnp5rmaqtyc07a33t9jdg88cjhhc92";
    const result = await transactionService.createTransaction(from, to, amount);
    return responseToClient(res, { data: result });
  } catch (error) {
    console.log(error);
    return next(error);
  }
};

/** ETH */
exports.estimateGas = async (req, res, next) => {
  try {
    const { to, amount } = req.query;
    const [gas, gasPrice] = await Promise.all([
      web3.eth.estimateGas({
        from: "0x9AE22FA828438A237C724c48962af01e3ed176Be",
        to,
        amount,
      }),
      axios
        .get(`${endpoints.Ropsten}/api`, {
          params: {
            module: "gastracker",
            action: "gasoracle",
            apikey: process.env.GASTRACKER_API_KEY,
          },
        })
        .then(function (response) {
          return response.data.result;
        }),
    ]);

    const { SafeGasPrice, ProposeGasPrice, FastGasPrice, suggestBaseFee } =
      gasPrice;
    const gweiToEther = new BigNumber(10).pow(9);
    return responseToClient(res, {
      gas,
      gasPrice,
      estimateFee: {
        safe: new BigNumber(gas * SafeGasPrice).dividedBy(gweiToEther),
        propose: new BigNumber(gas * ProposeGasPrice).dividedBy(gweiToEther),
        fast: new BigNumber(gas * FastGasPrice).dividedBy(gweiToEther),
        suggest: new BigNumber(gas * suggestBaseFee).dividedBy(gweiToEther),
      },
    });
  } catch (error) {
    next(error);
  }
};

exports.getETHTransactionDetail = async (req, res, next) => {
  try {
    const { txid } = req.query;
    const transaction = await web3.eth.getTransaction(txid);
    return responseToClient(res, {
      transaction,
    });
  } catch (error) {
    next(error);
  }
};

exports.createETHTransaction = async (req, res, next) => {
  try {
    const { to, amount } = req.body;
    const from = "0x9AE22FA828438A237C724c48962af01e3ed176Be";
    const privateKey =
      "0x49935e7ba07e5d82ae103a0f7724acbbb4eeb90fb5b90030f07b9c9a20ce88a9";

    const [networkId, gasPrice, gas, nonce, value] = await Promise.all([
      web3.eth.net.getId(),
      web3.eth.getGasPrice(),
      web3.eth.estimateGas({
        from,
        to,
      }),
      web3.eth.getTransactionCount(from),
      web3.utils.toWei(amount, "ether"),
    ]);

    /** sign contract */
    const signedTx = await web3.eth.accounts.signTransaction(
      { to, value, gasPrice, gas, nonce, chainId: networkId },
      privateKey
    );

    /** send transactionhiểu */
    const result = await web3.eth.sendSignedTransaction(
      signedTx.rawTransaction
    );

    return responseToClient(res, { result });
  } catch (error) {
    next(error);
  }
};

exports.createERC20Transaction = async (req, res, next) => {
  try {
    const { to, amount } = req.body;
    const from = process.env.ACCOUNT_ADDRESS;
    const privateKey = Buffer.from(process.env.ACCOUNT_PRIVATEKEY, "hex");

    const myContract = new web3.eth.Contract(ABI, contractAddress);
    // const result = await myContract.methods.balanceOf(from).call();
    const [gasPrice, nonce] = await Promise.all([
      axios
        .get(`${endpoints.Ropsten}/api`, {
          params: {
            module: "gastracker",
            action: "gasoracle",
            apikey: process.env.GASTRACKER_API_KEY,
          },
        })
        .then(function (response) {
          return response.data.result.suggestBaseFee;
        }),
      web3.eth.getTransactionCount(from),
    ]);
    const [nonceHex, gasPriceHex, gasHex, value] = await Promise.all([
      web3.utils.toHex(nonce),
      web3.utils.toHex(web3.utils.toWei(gasPrice, "gwei")),
      web3.utils.toHex(150000),
      web3.utils.toHex(web3.utils.toWei(amount, "ether")),
    ]);
    console.log("gasPrice", gasPrice);
    console.log("nonce", nonce);

    const rawTx = {
      from,
      nonce: nonceHex,
      gasPrice: gasPriceHex,
      gasLimit: gasHex,
      to: contractAddress,
      value: "0x0",
      data: myContract.methods.transfer(to, value).encodeABI(),
    };

    /**create transaction */
    const tx = new Tx(rawTx, { chain: "ropsten" });

    /**sign transaction */
    tx.sign(privateKey);
    const serializedTx = tx.serialize();

    /**send transaction */
    const result = await web3.eth
      .sendSignedTransaction("0x" + serializedTx.toString("hex"))
      .on("transactionHash", console.log);
    console.log("result", result);
    return responseToClient(res, { result });
  } catch (error) {
    next(error);
  }
};

/** XRP */
exports.estimateXRPFee = async (req, res, next) => {
  try {
    await api.connect();

    const feeStats = await api.connection.request({ command: "fee" });
    // const currentLedgerSize = Number(feeStats.current_ledger_size);
    // const expectedLedgerSize = Number(feeStats.expected_ledger_size);
    const medianFee = Number(feeStats.drops.median_fee);
    // const matrixLedgerSize = Math.round((expectedLedgerSize * 70) / 100);

    let fee = 0.000012;
    const expectedLedgerSize = 6;
    const currentLedgerSize = 8 - 1;
    const matrixLedgerSize = Math.round((expectedLedgerSize * 70) / 100);

    if (currentLedgerSize > matrixLedgerSize) {
      fee =
        (medianFee * Math.pow(currentLedgerSize, 2)) /
        Math.pow(expectedLedgerSize, 2);
    }

    // const result = await api.getFee();
    return responseToClient(res, {
      feeStats,
      currentLedgerSize,
      expectedLedgerSize,
      medianFee,
      matrixLedgerSize,
      fee,
    });
  } catch (error) {
    next(error);
  }
};

exports.createXRPTransaction = async (req, res, next) => {
  try {
    await api.connect();

    // Ripple payments are represented as JavaScript objects
    const payment = {
      source: {
        address: xrp_account[0].address,
        maxAmount: {
          value: "10.00",
          currency: "XRP",
        },
      },
      destination: {
        address: xrp_account[1].address,
        amount: {
          value: "10.00",
          currency: "XRP",
        },
      },
    };

    // Get ready to submit the payment
    const prepared = await api.preparePayment(xrp_account[0].address, payment, {
      maxLedgerVersionOffset: 10,
    });
    // Sign the payment using the sender's secret
    const { signedTransaction } = api.sign(
      prepared.txJSON,
      xrp_account[0].secret
    );
    console.log("Signed", signedTransaction);

    // Submit the payment
    const result = await api.submit(signedTransaction);
    return responseToClient(res, { result });
  } catch (error) {
    next(error);
  }
};

exports.getXRPTransactionDetail = async (req, res, next) => {
  try {
    const { tx } = req.query;
    await api.connect();

    const result = await api.getTransaction(tx);
    return responseToClient(res, { result });
  } catch (error) {
    next(error);
  }
};
