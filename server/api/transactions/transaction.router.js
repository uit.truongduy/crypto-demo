const express = require("express");
const transactionController = require("./transaction.controller");

const router = express.Router();

router.get("/", transactionController.getTransaction);
router.get("/estimateFee", transactionController.estimateFee);
router.post("/", transactionController.createTransaction);

/** ETH */
router.post("/estimateGas", transactionController.estimateGas);
router.get("/eth", transactionController.getETHTransactionDetail);
router.post("/eth", transactionController.createETHTransaction);
router.post("/erc20", transactionController.createERC20Transaction);

/** XRP */
router.post("/xrp", transactionController.createXRPTransaction);
router.get("/xrp/estimateFee", transactionController.estimateXRPFee);
router.get("/xrp", transactionController.getXRPTransactionDetail);

module.exports = router;
