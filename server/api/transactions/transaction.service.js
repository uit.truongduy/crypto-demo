const node = require("../../utils/node.bitcoin");
const BigNumber = require("bignumber.js");

exports.getTransactionDetail = async (txid, options = {}) => {
  if (typeof txid !== "string") throw new Error("txid must be string");

  const { include_watchOnly } = options;

  const transaction = await node.getTransaction(txid, include_watchOnly);
  if (!transaction) return { error: "transaction not found" };

  const { hex, blockhash } = transaction;
  const [decodehex, getblockheader] = await Promise.all([
    node.decodeScript(hex),
    node.getBlockHeader(blockhash),
  ]);
  return Object.assign(transaction, { decodehex, getblockheader });
};

exports.estimateFee = async (numInput, numOutput) => {
  const fee = await node.estimateSmartFee(6);
  return (fee.feerate * (numInput * 180 + numOutput * 34 + 10)) / 1000;
};

// exports.createRawTransaction = async (
//   inputs,
//   outputs,
//   locktime = 0,
//   replaceable = false
// ) => {
//   try {
//     if (!Array.isArray(inputs) || !Array.isArray(outputs))
//       throw new Error("inputs and outputs must be array");
//     if (inputs.length < 1 || outputs.length < 1)
//       throw new Error("inputs and outputs must have element");

//     const inputElementObject = inputs.every((i) => typeof i === "object");
//     if (!inputElementObject) throw new Error("inputs element must be object");

//     const outElementObject = outputs.every((o) => typeof o === "object");
//     if (!outElementObject) throw new Error("outputs element must be object");

//     return await node.createRawTransaction(
//       inputs,
//       outputs,
//       locktime,
//       replaceable
//     );
//   } catch (error) {
//     return error;
//   }
// };

// exports.signRawTransactionWithKey = async (
//   hexString,
//   passphrase,
//   address,
//   sighashtype = "ALL"
// ) => {
//   try {
//     if (
//       typeof hexString !== "string" ||
//       typeof passphrase !== "string" ||
//       typeof address !== "string"
//     )
//       throw new Error("hexString, passphrase, address must be string");
//     await node.walletPassphrase(passphrase, +process.env.PASSPHRASE_TIMEOUT);
//     const privkey = await node.dumpPrivKey(address);
//     return await node.signRawTransactionWithKey(
//       hexString,
//       [privkey],
//       null,
//       sighashtype
//     );
//   } catch (error) {
//     return error;
//   }
// };

// exports.sendRawTransaction = async (hexstring, allowhighfees = false) => {
//   try {
//     if (typeof hexstring !== "string")
//       throw new Error("hexString must be string");

//     return await node.sendRawTransaction(hexstring, allowhighfees);
//   } catch (error) {
//     return error;
//   }
// };

exports.createTransaction = async (from, to, amount) => {
  try {
    if (from === to) return { error: "from & to must difference" };

    /** get list unspent by address */
    const listUnspent = await node.listUnspent(null, null, [from]);
    listUnspent.sort((a, b) => b.amount - a.amount);

    /** calculate fee & inputs */
    let total = new BigNumber(0);
    let fees = new BigNumber(0);
    const bAmount = new BigNumber(amount);
    const inputs = [];

    const length = listUnspent.length;
    for (let i = 0; i < length; i++) {
      fees = new BigNumber((await this.estimateFee(i, 2)) + "");
      const { txid, vout, scriptPubKey, amount } = listUnspent[i];
      inputs.push({
        txid,
        vout,
        scriptPubKey,
      });
      total = total.plus(amount);
      if (total >= bAmount.plus(fees)) break;
    }

    if (total < bAmount.plus(fees)) {
      return { error: "Amount not enough" };
    }

    /** create outputs */
    const outputs = [
      {
        [to]: amount,
      },
    ];

    /** excess cash > fees refund from*/
    const excessCash = total.minus(bAmount.plus(fees));
    if (excessCash > fees) {
      outputs.push({
        [from]: +excessCash.toString(),
      });
    }

    console.log("outputs", outputs);
    /** create raw transaction */
    const hexString = await node.createRawTransaction(inputs, outputs);

    /** sign raw transaction */
    await node.walletPassphrase(
      "my pass phrase",
      +process.env.PASSPHRASE_TIMEOUT
    );
    const privkey = await node.dumpPrivKey(from);
    const signHex = await node.signRawTransactionWithKey(hexString, [privkey]);

    /** send raw transaction */
    return await node.sendRawTransaction(signHex.hex);
  } catch (error) {
    return error;
  }
};
