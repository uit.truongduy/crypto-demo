const { responseToClient } = require("../../utils/response.util");
const node = require("../../utils/node.bitcoin");
const web3 = require("../../utils/node.eth");
const api = require("../../utils/node.xrp");
const xrp_account = require("../../../data/xrp_account.json");

require("dotenv").config();

exports.getBalance = async (req, res, next) => {
  try {
    const balance = await node.getBalance();
    return responseToClient(res, { balance });
  } catch (error) {
    next(error);
  }
};

exports.getBalanceByAddress = async (req, res, next) => {
  try {
    const { address } = req.params;
    const balance = await node.getBalanceByAddress((addresses = [address]));
    return responseToClient(res, { balance });
  } catch (error) {
    next(error);
  }
};

exports.getNewAddress = async (req, res, next) => {
  try {
    const getNewAddress = await node.getNewAddress("label", "my pass phrase");
    return responseToClient(res, getNewAddress);
  } catch (error) {
    next(error);
  }
};

exports.listLabels = async (req, res, next) => {
  try {
    const listLabels = await node.listLabels();
    return responseToClient(res, { listLabels });
  } catch (error) {
    next(error);
  }
};

/** ETH */
exports.createETHWallet = async (req, res, next) => {
  try {
    const wallet = await web3.eth.accounts.wallet.create(0);
    console.log(wallet);
    return responseToClient(res, { message: "create wallet success" });
  } catch (error) {
    next(error);
  }
};

exports.addETHWallet = async (req, res, next) => {
  try {
    const { account } = req.query;
    const result = await web3.eth.accounts.wallet.add(account);
    console.log(result);
    return responseToClient(res, result);
  } catch (error) {
    next(error);
  }
};

exports.getETHBalance = async (req, res, next) => {
  try {
    const { address } = req.query;
    const wei = await web3.eth.getBalance(address);
    const balance = await web3.utils.fromWei(wei, "ether");
    return responseToClient(res, {
      balance,
    });
  } catch (error) {
    next(error);
  }
};

/** XRP */
exports.getXRPInfo = async (req, res, next) => {
  try {
    await api.connect();
    const info = await api.getAccountInfo(xrp_account[0].address);
    return responseToClient(res, {
      info,
    });
  } catch (error) {
    next(error);
  }
};
