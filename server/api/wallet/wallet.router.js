const express = require("express");
const walletValidate = require("./wallet.validate");
const walletController = require("./wallet.controller");

const router = express.Router();

router.get("/balance", walletController.getBalance);

router.get(
  "/balance/:address",
  walletValidate.getBalanceValidate,
  walletController.getBalanceByAddress
);

router.get("/getNewAddress", walletController.getNewAddress);

router.get("/listLabels", walletController.listLabels);

/** ETH */

router.post("/eth", walletController.createETHWallet);

router.post("/eth/account", walletController.addETHWallet);

router.get("/eth/balance", walletController.getETHBalance);

/** XRP */
router.get("/xrp/info", walletController.getXRPInfo);

module.exports = router;
