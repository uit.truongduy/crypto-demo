exports.getBalanceValidate = function (req, res, next) {
  const { address } = req.params;
  if (typeof address !== "string") return next(new Error("address must be string"));
  return next();
}

exports.getNewAddress = function (req, res, next) {
  const { label } = req.query;
  if (typeof label !== "string") return next(new Error("label must be string"));
  return next();
}