const express = require("express");
const wallet = require("./api/wallet/wallet.router");
const transactions = require("./api/transactions/transaction.router");
const account = require("./api/accounts/accounts.router");

const router = express.Router();
router.use("/wallet", wallet);
router.use("/transaction", transactions);
router.use("/eth/account", account);

module.exports = router;
