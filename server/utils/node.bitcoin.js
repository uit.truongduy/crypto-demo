const Client = require("bitcoin-core");
const config = require("../../configs/bitcoin-core.config");

const { network, host, port, username, password } = config;

class Node {
  constructor({ username, password, network, host, port }) {
    this._network = network;
    this._host = host;
    this._port = port;
    this._username = username;
    this._password = password;

    this._client = new Client({
      network,
      host,
      port,
      username,
      password,
    });
  }

  async getBalance() {
    return await this._client.getBalance("*", 0);
  }

  async getBalanceByAddress(address) {
    const listUnspent = await this._client.listUnspent();
    const balance = listUnspent.reduce((acc, cur) => {
      return cur.address === address ? acc + cur.amount : acc;
    }, 0);
    return balance;
  }

  async getNewAddress(label, passphrase = null) {
    const newAddress = await this._client.getNewAddress(label);

    if (passphrase) {
      await this._client.walletPassphrase(
        passphrase,
        +process.env.PASSPHRASE_TIMEOUT
      );
    }
    const privKey = await this._client.dumpPrivKey(newAddress);
    return { newAddress, privKey };
  }

  async getWalletInfo() {
    return this._client.getWalletInfo();
  }

  async listLabels() {
    return await this._client.listLabels();
  }

  async listUnspent(
    minconf = 1,
    maxconf = 9999999,
    addresses = [],
    include_unsafe = true,
    query_options = {}
  ) {
    return await this._client.listUnspent(
      minconf,
      maxconf,
      addresses,
      include_unsafe,
      query_options
    );
  }

  async dumpPrivKey(address) {
    return await this._client.dumpPrivKey(address);
  }

  async dumpWallet(filename) {
    return this._client.dumpWallet(filename);
  }

  async encryptWallet(passphrase) {
    return await this._client.encryptWallet(passphrase);
  }

  async walletPassphrase(passphrase) {
    return await this._client.walletPassphrase(
      passphrase,
      +process.env.PASSPHRASE_TIMEOUT
    );
  }

  /**transaction */
  async createRawTransaction(
    inputs,
    outputs,
    locktime = 0,
    replaceable = false
  ) {
    return await this._client.createRawTransaction(
      inputs,
      outputs,
      locktime,
      replaceable
    );
  }

  async combineRawTransaction(hexstring) {
    return await this._client.combineRawTransaction(hexstring);
  }

  async getTransaction(txid, include_watchOnly = false) {
    return await this._client.getTransaction(txid, include_watchOnly);
  }

  async decodeScript(hexString) {
    return await this._client.decodeScript(hexString);
  }

  async getBlockHash(height) {
    return await this._client.getBlockHash(height);
  }

  async getBlockHeader(blockHash) {
    return await this._client.getBlockHeader(blockHash);
  }

  /** fee */
  async estimateSmartFee(conf_target, estimate_mode = "CONSERVATIVE") {
    return await this._client.estimateSmartFee(conf_target, estimate_mode);
  }

  async estimateRawFee(conf_target, threshold = 0.95) {
    return await this._client.estimateRawFee(conf_target, threshold);
  }

  async signRawTransactionWithKey(
    hexString,
    privkeys,
    prevtxs = null,
    sighashtype = "ALL"
  ) {
    return await this._client.signRawTransactionWithKey(
      hexString,
      privkeys,
      prevtxs,
      sighashtype
    );
  }

  async sendRawTransaction(hexstring, maxfeerate = 0.1) {
    return await this._client.sendRawTransaction(hexstring);
  }
}

exports.Node = Node;
module.exports = new Node({
  network,
  host,
  port,
  username,
  password,
});
