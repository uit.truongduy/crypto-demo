const Web3 = require("web3");
const web3 = new Web3();

web3.setProvider(
  new web3.providers.WebsocketProvider(
    "wss://ropsten.infura.io/ws/v3/f37d9f2929034a058b7d5f55b6cbb98e",
    {
      clientConfig: {
        // Useful to keep a connection alive
        keepalive: true,
        keepaliveInterval: 60000, // ms
      },
      // Enable auto reconnection
      reconnect: {
        auto: true,
        delay: 5000, // ms
        maxAttempts: 5,
        onTimeout: false,
      },
    }
  )
);

module.exports = web3;
