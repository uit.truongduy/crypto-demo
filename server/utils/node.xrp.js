const RippleAPI = require("ripple-lib").RippleAPI;

const api = new RippleAPI({
  server: "wss://s.altnet.rippletest.net",
});
api.connection._config.connectionTimeout = 3e4;
module.exports = api;
