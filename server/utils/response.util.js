exports.responseToClient = (res, data, options = {}) => {
  const status = options.status || 200;
  return res.status(status).json(data);
}